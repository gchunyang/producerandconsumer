#include "WorkShop.h"

int main(int argc, char const *argv[])
{
    WorkShop shop(5, 100, 150);
    shop.startWorking();

    return 0;
}
